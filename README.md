# PROJECT INFO
This is a Simple File sharing Web Application 
Here we can upload files, images, etc, and share with another person so that he/she can download that file

## Dir infomation
Dockerfile - To locally custom-build a File-sharing web-app container
Docker-compose - Clone and run a File-sharing web-app container (image used from dockerhub)
projects3 - Folder where that application file stored with the upload folder where all the upload files are stored/saved locally 

## How to USE

Install docker & docker-compose
- apt install docker.io -y 
- apt install docker-compose -y 

Clone the Project
- git clone https://gitlab.com/self-made-projects/projects3-compose.git
- cd projects3-compose

RUN the project
- docker-compose up -d 

URL:  
http://localhost:8555/ 
- public port (8555) can be changed in the docker-compose.yml file
