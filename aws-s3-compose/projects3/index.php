<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>File Upload</title>
    <style>
        /* Style for the video background */
        body {
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
            background: #000;
            overflow: hidden;
        }
        
        video {
            position: absolute;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            transform: translate(-50%, -50%);
            z-index: -1;
        }

        /* Style for the main content */
        .content {
            position: relative;
            text-align: center;
            color: #fff;
            z-index: 1;
        }

        h1 {
            font-size: 3em;
            margin-top: 0;
            padding-top: 50px;
        }

        /* Style for the upload form */
        form {
            margin: 20px auto;
            padding: 20px;
            background: rgba(0, 0, 0, 0.6);
            border-radius: 10px;
            display: inline-block;
        }

        input[type="file"] {
            display: none;
        }

        label {
            cursor: pointer;
            background: #3498db;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            font-size: 1.2em;
        }

        /* Style for the stored files list */
        ul {
            list-style: none;
            padding: 0;
        }

        li {
            margin: 10px 0;
            font-size: 1.2em;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        /* Style for the emoji art */
        .emoji-art {
            font-size: 2em;
        }
    </style>
</head>
<body>
    <video autoplay muted loop>
        <source src="your-video.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video>

    <div class="content">
        <h1>Welcome to Our S3 File Upload </h1>
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <label for="file">Select a File</label>
            <input type="file" name="file" id="file" required>
            <br><br>
            <input type="submit" name="submit" value="Upload">
        </form>

        <h2>Stored Files</h2>
        <ul>
            <?php include 'list_files.php'; ?>
        </ul>

        <div class="emoji-art">
            &#127775; &#128293; &#128226; &#128077; &#128640;
        </div>
    </div>
</body>
</html>

